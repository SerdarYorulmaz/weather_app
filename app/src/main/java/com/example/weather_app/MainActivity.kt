package com.example.weather_app

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    var tvSehir: TextView? = null
    var locationManager: LocationManager? = null
    var latitude: String? = null
    var longtitude: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        try {
            // Request location updates
            locationManager?.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                0L,
                0f,
                locationListener
            )
        } catch (ex: SecurityException) {
            Log.d("myTag", "Security Exception, no location available")
        }


        var spinnerAdapter =
            ArrayAdapter.createFromResource(this, R.array.cityName, R.layout.spinner_only_line)

        //yanindaki spin isareti icin
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_only_line)
        spnCity.background.setColorFilter(
            resources.getColor(R.color.colorPrimaryDark),
            PorterDuff.Mode.SRC_ATOP
        ) //Renk degistirmek icin spinner okunu
        spnCity.adapter = spinnerAdapter

//        spnCity.setTitle("Şehir Seç") //hazir companent ozellikleri
//        spnCity.setPositiveButton("SEÇ")


        spnCity.setOnItemSelectedListener(this)


        spnCity.setSelection(1)
        sourceData("Antalya")
    }
//TODO bURDAYIZ
    fun currentCity(lat: String?, long: String?): String? {
        Log.e("SONUC","${lat}  ${long}")
        var url =
            "https://api.openweathermap.org/data/2.5/find?lat=${lat?.toDouble()}&lon=${long?.toDouble()}&appid=0c204b4e35bac43071aa0e36c40b9547&lang=tr&units=metric"
//http://api.openweathermap.org/data/2.5/find?lat=55.5&lon=37.5&cnt=10
        //http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric

        var cityName: String? = null

        var requestA = JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            object : Response.Listener<JSONObject> {
                override fun onResponse(response: JSONObject?) {


                    var list = response?.getJSONArray("list")
                    cityName = list?.getJSONObject(0)?.getString("name")

                    var main = list?.getJSONObject(0)?.getJSONObject("main")
                    var temperature = main?.getInt("temp")

                    tvSehir?.setText(cityName.toString())

                    var weather = list?.getJSONObject(0)?.getJSONArray("weather")
                    var description = weather?.getJSONObject(0)?.getString("description")

                    txtDescription.setText(description.toString())

                    //internet resim eklemek icinde
                    var icon = weather?.getJSONObject(0)?.getString("icon")

//                icon?.let { ic->
//                    isNight(ic)
//                }

                    rootLayout.background = isNight2(icon.toString())
                    var imgFileName = resources.getIdentifier(
                        "icon_${icon?.lastCaracterDelete()}",
                        "drawable",
                        packageName
                    ) //R.drawable.icon_50n tipinde
                    imgWeather.setImageResource(imgFileName)

                    txtTemperature.setText(temperature.toString())

                    txtDate.setText(datePrint())

//                Toast.makeText(this@MainActivity,
//                    "Gelen Data: sıcaklık: ${temperature.toString()} sehir: ${cityName} hava nasıl: ${description} icon:",Toast.LENGTH_LONG).show()
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError?) {
                    Toast.makeText(this@MainActivity, "HATA ", Toast.LENGTH_LONG).show()
                }
            })


        MySingleton.getInstance(this)?.addToRequestQueue(requestA)

        if (cityName != null) {
            return cityName
        } else return "N \\ A"


    }


    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {

            Log.d("myTag", "fasd" + latitude + ":" + longtitude)
            latitude = String.format("%.2f", location?.latitude)
            longtitude = String.format("%.2f", location?.longitude)
            txtGpsY.text = longtitude
            txtGpsX.text = latitude
            Log.e("TEST","${longtitude} ${latitude}")
            currentCity(latitude, longtitude)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    //spinner secme durumlarinda
    override fun onNothingSelected(parent: AdapterView<*>?) {
        //Bir sey yapılmadiginda tetiklenecek method
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        tvSehir = view as TextView

        if (position == 0) {
            var currentName = currentCity(latitude, longtitude)

            if (currentName != null) {
                txtCity?.setText(currentName)
            }
            Toast.makeText(this@MainActivity, "Gelen Data: " + currentName, Toast.LENGTH_LONG)
                .show()
        } else {
            var choseCity = parent?.getItemAtPosition(position).toString()
            tvSehir = view as TextView
            sourceData(choseCity)
        }


    }


    fun sourceData(city: String) {
        //        //volley data alma islemi
//        //String Request
//        var request=StringRequest(Request.Method.GET,"https://www.google.com/ ",object : Response.Listener<String>{
//            override fun onResponse(response: String?) {
//                Toast.makeText(this@MainActivity,"Gelen Data: "+response,Toast.LENGTH_LONG).show()
//            }
//        },object:Response.ErrorListener{
//            override fun onErrorResponse(error: VolleyError?) {
//                Toast.makeText(this@MainActivity,"HATA ",Toast.LENGTH_LONG).show()
//            }
//        })
//
//
//        MySingleton.getInstance(this)?.addToRequestQueue(request )


        var url =
            "https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=0c204b4e35bac43071aa0e36c40b9547&lang=tr&units=metric"

        var request = JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            object : Response.Listener<JSONObject> {
                override fun onResponse(response: JSONObject?) {

                    var main = response?.getJSONObject("main")
                    var temperature = main?.getInt("temp")
                    var cityName = response?.getString("name")

                    txtCity.setText(cityName.toString())

                    var weather = response?.getJSONArray("weather")
                    var description = weather?.getJSONObject(0)?.getString("description")

                    txtDescription.setText(description.toString())

                    //internet resim eklemek icinde
                    var icon = weather?.getJSONObject(0)?.getString("icon")

//                icon?.let { ic->
//                    isNight(ic)
//                }

                    rootLayout.background = isNight2(icon.toString())
                    var imgFileName = resources.getIdentifier(
                        "icon_${icon?.lastCaracterDelete()}",
                        "drawable",
                        packageName
                    ) //R.drawable.icon_50n tipinde
                    imgWeather.setImageResource(imgFileName)

                    txtTemperature.setText(temperature.toString())

                    txtDate.setText(datePrint())

//                Toast.makeText(this@MainActivity,
//                    "Gelen Data: sıcaklık: ${temperature.toString()} sehir: ${cityName} hava nasıl: ${description} icon:",Toast.LENGTH_LONG).show()
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError?) {
                    Toast.makeText(this@MainActivity, "HATA ", Toast.LENGTH_LONG).show()
                }
            })


        MySingleton.getInstance(this)?.addToRequestQueue(request)
    }

    fun isNight(v: String) {
        if (v?.last() == 'd') {
            rootLayout.background = getDrawable(R.drawable.bg)
        } else {
            rootLayout.background = getDrawable(R.drawable.gece)
        }
    }

    fun isNight2(v: String): Drawable? {
        return if (v?.last() == 'd') {
            getDrawable(R.drawable.bg)
        } else {
            getDrawable(R.drawable.gece)
        }
    }

    fun datePrint(): String { //serviste yok direk sistemden alacagiz
        var calender = Calendar.getInstance().time
        var formatDate = SimpleDateFormat("EEEE,MMMM yyyy", Locale("tr"))
        var date = formatDate.format(calender)

        return date.toString()
    }

    fun String.lastCaracterDelete(): String {
// 50n --> 50 son karakteri siler
        return this.substring(0, this.length - 1)
    }


}




